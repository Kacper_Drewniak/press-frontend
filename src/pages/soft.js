import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/main.scss";
import { Link } from "gatsby";
import Nav from "../components/nav";
import "../assets/css/soft.scss";
import Footer from "../components/footer";

const soft = () => {
    const data = useStaticQuery(query);
    const strapiHost = process.env.GATSBY_API_URL;
    const soft = data.allStrapiSoft.nodes[0].software_single

    return <>
    <Nav/>
    <div className="soft__section">
        <div className="soft__left">
            <h1 className="soft__title">{data.strapiSoft.title}</h1>
            <p className="soft__description">{data.strapiSoft.description}</p>
        </div>
        <div className="soft__right">
            <img src={`${strapiHost}${data.strapiSoft.img.url}`} className="soft__img"/>
        </div>
    </div>
    <div className="soft__single">
        <div className="soft__single-wrapper">
    {soft.map(({description, img, title}) => 
    
            < div className="soft__single-offer">
                <h2 className="soft__single-title">{title}</h2>
                <img src={`${strapiHost}${img.url}`} className="soft__img2"/>  
                <p className="soft__single-desc1">{description}</p>
            </div>)}
        </div>
    </div>
    <button
                className="soft__button"
                type="button"
                ><Link to="/">
                Zapytaj o bezpłatną wycenę</Link>
                </button>
    <Footer/>
    </>
};

    const query = graphql`
    query {
        strapiSoft {
            description
            title
            img {
                url
            }
        }
        allStrapiSoft {
            nodes {
                software_single {
                    description
                    title
                    img {
                    url
                        }
                    }
                }
            }

    }`;
    export default soft;