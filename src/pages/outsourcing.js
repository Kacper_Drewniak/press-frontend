import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/main.scss";
import { Link } from "gatsby";
import Nav from "../components/nav";
import Footer from "../components/footer";
import "../assets/css/outsourcing.scss";
import Checkboxes from "../components/checkboxes";
const outsourcing = () => {
    const data = useStaticQuery(query);
    const strapiHost = process.env.GATSBY_API_URL;
    const out = data.allStrapiOutsourcing.nodes[0].outsourcing_single

    return <> 
    <Nav/>
    <div className="outsourcing__section1">
        <div className="outsourcing__section1-left">
            <img src={`${strapiHost}${data.strapiOutsourcing.img.url}`}/>
        </div>
        <div className="outsourcing__section1-right">
            <h1 className="outsourcing__section1-title">{data.strapiOutsourcing.title}</h1>
            <p className="outsourcing__section1-description">{data.strapiOutsourcing.description}</p>
        </div>
    </div>
    
    <div className="outsourcing__section2">
        <h2 className="outsourcing__section2-title">{data.strapiOutsourcing.title_section2}</h2>
        <div className="outsourcing__section2-content">
        {out.map(({description, number, title}) => 
            <div className="outsourcing__single">
                <h2 className="outsourcing__single-title">{title}</h2>
                <p className="outsourcing__single-description">{description}</p>
                <p className="outsourcing__single-number">{number}</p>
            </div>
    )}
        </div>
    </div>
    <button
                className="outsourcing__button"
                type="button"
                ><Link to="/">
                Zapytaj o bezpłatną wycenę</Link>
                </button>

    <Checkboxes/>
    <Footer/>

    </>
}
const query = graphql`{
    allStrapiOutsourcing {
        nodes {
            outsourcing_single {
            description
            number
            title
            }
        }
        }
    strapiOutsourcing {
        title
        title_section2
        description
        img {
            url
        }
    }
}`;

export default outsourcing;