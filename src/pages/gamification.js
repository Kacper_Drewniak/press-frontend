import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/main.scss";
import { Link } from "gatsby";
import Nav from "../components/nav";
import "../assets/css/gamification.scss";
import Footer from "../components/footer";

const gamification = () => {
    const data = useStaticQuery(query);
    const strapiHost = process.env.GATSBY_API_URL;
    const game = data.allStrapiGamification.nodes[0].realizations_gamification
    return <>
    <Nav/>
    <div className="gamification__wrapper">
        <div className="gamification__first">
        <div className="gamification__left">
            <img src={`${strapiHost}${data.strapiGamification.img.url}`}/>
        </div>
        <div className="gamification__right">
            <h1 className="gamification__title">{data.strapiGamification.title_gamification}</h1>
            <p className="gamification__description">{data.strapiGamification.description_gamification}</p>
        </div>
        </div>
        
        
        
    </div>
    <div className="gamification__section2">
        <p className="gamification__description2">{data.strapiGamification.description}</p>
        <div className="gamification__rectangles">
                {game.map(({description, img}) => <div className="gamification__rec"><img src={`${strapiHost}${img.url}`} className="homepage__img2"/>
                <p className="gamification__desc3">{description}</p>
                </div>)}
        </div>
    </div>
    <button
                className="gamification__button"
                type="button"
                ><Link to="/">
                Zapytaj o bezpłatną wycenę</Link>
                </button>

    <Footer/>
    </>

};

const query = graphql`
    query {
        strapiGamification {
            title_gamification
            description_gamification
            description
            img {
            url
            }
        }
        allStrapiGamification {
            nodes {
            realizations_gamification {
                description
                img {
                url
                }
            }
            }
        }
    }
`;

export default gamification;