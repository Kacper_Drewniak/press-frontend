import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/main.scss";
import { Link } from "gatsby";
import Nav from "../components/nav";
import "../assets/css/realizations.scss";
import Footer from "../components/footer";
import "../assets/css/shops.scss";

const shops = () => {
    const data = useStaticQuery(query);
    const shop = data.allStrapiShops.nodes[0].shops_single
    const strapiHost = process.env.GATSBY_API_URL;
    const shop2 = data.allStrapiShops.nodes[0].shops_realizations


    return <>
    <Nav/>
    <div className="shops__wrapper">
        <div className="shops__section1">
            <div className="shops__section1-left">
                <img src={`${strapiHost}${data.strapiShops.img.url}`} className="shops__img"/>
            </div>
            <div className="shops__section1-right">
                <h1 className="shops__section1-title">{data.strapiShops.title}</h1>
                <p className="shops__section1-description">{data.strapiShops.description}</p>
            </div>
        </div>
        <div className="shops__section2">
            <div className="shops__section2-content">
                {shop.map(({description, img, title}) => 
                <div className="shops__section2-single">
                    <img src={`${strapiHost}${img.url}`} className="shops__img2"/>
                    <h2 className="shops__section2-title">{title}</h2>
                    <p className="shops__section2-description">{description}</p>
                </div>)}
            </div>
        </div>
        <div className="shops__section3">
            <div className="shops__section3-left">
                <img src={`${strapiHost}${data.strapiShops.img_presta.url}`} className="shops__img-presta"/>
            </div>
            <div className="shops__section3-right">
                <h2 className="shops__section3-title">{data.strapiShops.title_presta}</h2>
                <p className="shops__section3-description">{data.strapiShops.description_presta}</p>
            </div>
        </div>
        <div className="shops__section4">
            <h2 className="shops__section4-title">{data.strapiShops.title_realizations}</h2>
            <div className="shops__section4-content">
            {shop2.map(({img, title}) => 
                <div className="shops__section4-single">
                    <img src={`${strapiHost}${img.url}`} className="shops__img3"/>
                    <h2 className="shops__section4-title2">{title}</h2>
                </div>)}
                </div>        
            </div>
            <button
                className="shops__button"
                type="button"
                ><Link to="/">
                Zapytaj o bezpłatną wycenę</Link>
                </button>
    </div>
    <Footer/>
    </>
};

const query = graphql`{
    strapiShops {
        description
        description_presta
        img {
            url
        }
        img_presta {
            url
        }
        title
        title_presta
        title_realizations
        }

    allStrapiShops {
        nodes {
            shops_realizations {
            img {
                url
            }
            title
            }
            shops_single {
            description
            img {
                url
            }
            title
            }
        }
        }
}`;
export default shops;