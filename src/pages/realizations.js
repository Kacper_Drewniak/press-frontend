import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/main.scss";
import { Link } from "gatsby";
import Nav from "../components/nav";
import "../assets/css/realizations.scss";
import Footer from "../components/footer";

const realizations = () => {
    const data = useStaticQuery(query);
    const realization = data.allStrapiRealizations.nodes[0].realization_single
    const strapiHost = process.env.GATSBY_API_URL;


    return <>
    <Nav/>
    <h1 className="realizations__title">{data.strapiRealizations.title}</h1>
    <p className="realizations__desc">{data.strapiRealizations.description}</p>
    <div className="realizations__wrapper">
    {realization.map(({title, img, description_back}) => <div className="realizations__single">
        <img src={`${strapiHost}${img.url}`} className="realizations__img2"/>
        <div className="realizations__single-back">
            <p>{description_back}</p>
        </div>
        <h2 className="realizations__des">{title}</h2>
            </div>)}
                </div>

    <div className="realizations__section2">
        <div className="realizations__section2-left">
        </div>
        <div className="realizations__section2-right">
            <h2 className="realizations__title2">{data.strapiRealizations.title2}</h2>
            <p className="realizations__description3">{data.strapiRealizations.description3}</p>

        </div>
        
    </div>

    <button
                className="realizations__button"
                type="button"
                ><Link to="/">
                Zapytaj o bezpłatną wycenę</Link>
                </button>
    <Footer/>
    </>
}

const query = graphql`
query {
    allStrapiRealizations {
        nodes {
            realization_single {
            title
            description_back
            img {
                url
                }
            }
        }
    }
    strapiRealizations {
        title
        title2
        description
        description3
    }
}`;


export default realizations;