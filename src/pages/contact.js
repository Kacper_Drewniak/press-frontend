// import React from "react";
// import { graphql, useStaticQuery } from "gatsby";
// import "../assets/css/main.scss";
// import { Link } from "gatsby";
// import Nav from "../components/nav";
// import "../assets/css/contact.scss";
// import Footer from "../components/footer";
// import { Formik, Field, Form } from 'formik';

// const contact = () => {
//     const data = useStaticQuery(query);
//     const strapiHost = process.env.GATSBY_API_URL;
//     const contactSingle = data.allStrapiContact.nodes[0].contact_single;

//     return <>
//     <Nav/>
//     <div className="contact__wrapper">
//         <div className="contact__content">
//             <div className="contact__section1">
//                 <div className="contact__left">
//                     <h1 className="contact__left-title">{data.strapiContact.title}</h1>
//                     <h2 className="contact__left-description">{data.strapiContact.description}</h2>
//                     <div className="contact__singles">
//                         {contactSingle.map(({description_single, img}) => 
//                         <div className="contact__single">
//                             <img src={`${strapiHost}${img.url}`}/>
//                             <p className="contact__single-description">{description_single}</p>
//                         </div>)}
//                     </div>
//                 </div>
//                 <div className="contact__right">
//                     <img src={`${strapiHost}${data.strapiContact.img.url}`}/>
//                 </div>
//             </div>

            
//         <Formik className="contact__form"
//         initialValues={{
//         firstName: '',
//         phone: '',
//         company: '',
//         email: '',
//         }}
//         onSubmit={async (values) => {
//         await new Promise((r) => setTimeout(r, 500));
//         alert(JSON.stringify(values, null, 2));
//         }}
//         >
//         <Form className="contact__form">
//         <label htmlFor="firstName" className="contact__form-name">Imię i nazwisko</label>
//         <Field id="firstName" name="firstName" />
        
//         <label htmlFor="email" className="contact__form-email">Email</label>
//         <Field
//             id="email"
//             name="email"
//             type="email"
//         />
//         <label htmlFor="company" className="contact__form-company">Nazwa firmy(opcjonalnie)</label>
//         <Field id="company" name="company"/>

//         <label htmlFor="phone" className="contact__form-phone">Telefon</label>
//         <Field type="phone" name="phone" id="phone" />

//         <label className="contact__form-subject"> Temat </label>
//         <Field type="text" name="subject" id="subject" />
                    
//         <label className="contact__form-message">Treść zapytania </label>
//         <Field type="text" name="message" id="message"/>
        
//         <button type="submit" className="contact__form-button">Wyślij zapytanie</button>
//         </Form>
//     </Formik>
//         </div>
//     </div>
//     </>
// }

// const query = graphql`
//     query {
//     strapiContact {
//         description
//         title
//         img {
//         url
//         }
//     }
//         allStrapiContact {
//             nodes {
//                 contact_single {
//                 description_single
//                 img {
//                 url
//                 }
//             }
//         }
//     }
// }`;

// export default contact;