import React from "react";
import { graphql, useStaticQuery } from "gatsby";
import "../assets/css/footer.scss";

const footer = () => {
    return <>
    <div className="footer__wrapper">
        <p className="footer__logo">Pressly.</p>
    </div>
    </>
};

export default footer;