import React from "react";
import { Link, StaticQuery, graphql } from "gatsby";

const Clutch = () => {
    return <>
    <script type="text/javascript" src="https://widget.clutch.co/static/js/widget.js">
        </script> 
    <div class="clutch-widget" data-url="https://widget.clutch.co" data-widget-type="8" data-expandifr="true" data-height="auto" data-clutchcompany-id="1593275">

    </div>
    </>
}

export default Clutch;